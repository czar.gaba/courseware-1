﻿Imports System.Data.OleDb
Public Class UC_StudentManagement
    Dim sql As String
    Sub update12()


        connection()

            If txtpass.Text = txtrepass.Text Then

            sql = "Update tbl_user set `password`='" & txtpass.Text & "', `name`='" & txtname.Text & "', `course`='" & txtcourse.Text & "'WHERE username='" & txtuser.Text & "'"
            cm = New OleDbCommand(sql, module1.conn)
            Dim i As Integer
            i = cm.ExecuteNonQuery

            If i > 0 Then
                txtuser.Clear()
                txtpass.Clear()
                txtrepass.Clear()
                txtname.Clear()
                txtcourse.Clear()
                btnedit.Enabled = False
                btndel.Enabled = False

                Button4.PerformClick()
            End If


        Else
            MsgBox("Password Did not Match Re-enter Passsword")

            Dim row As DataGridViewRow = DataGridView1.CurrentRow
            txtuser.Text = row.Cells(1).Value.ToString
            txtpass.Text = row.Cells(2).Value.ToString
            txtrepass.Text = row.Cells(2).Value.ToString
            txtname.Text = row.Cells(4).Value.ToString()
            txtcourse.Text = row.Cells(5).Value.ToString()
            Button4.PerformClick()

        End If

    End Sub
    Sub del()
        Try

            connection()
            sql = ("DELETE FROM tbl_user WHERE username = '" & txtuser.Text & "'")
            cm = New OleDbCommand(sql, module1.conn)
            Dim i As Integer
            i = cm.ExecuteNonQuery

            If i > 0 Then

                MsgBox("User Deleted!", MsgBoxStyle.Critical, "Add Employee")
                txtuser.Clear()
                txtpass.Clear()
                txtrepass.Clear()
                txtname.Clear()
                txtcourse.Clear()
                Button4.PerformClick()
                btnedit.Enabled = False
                btndel.Enabled = False
            Else
                MsgBox("Error on deleting data!")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub add()
        Try
            connection()
            da = New OleDbDataAdapter("SELECT * FROM tbl_user WHERE username='" & txtuser.Text & "'", module1.conn)
            ds = New DataSet()
            da.Fill(ds, "tbl_user")

            If ds.Tables("tbl_user").Rows.Count = 0 Then


                If txtpass.Text = txtrepass.Text Then



                    sql = "Insert Into tbl_user (`username`, `password`, `access`, `name`, `course`)Values('" & txtuser.Text & "','" & txtpass.Text & "','stud','" & txtname.Text & "','" & UCase(txtcourse.Text) & "')"

                    cm = New OleDbCommand(sql, module1.conn)
                    Dim i As Integer
                    i = cm.ExecuteNonQuery
                    If i > 0 Then

                        txtuser.Clear()
                        txtpass.Clear()
                        txtname.Clear()
                        txtcourse.Clear()
                        txtrepass.Clear()
                        Button4.PerformClick()

                    Else
                        txtpass.Clear()
                        txtrepass.Clear()
                        txtuser.Clear()
                        txtuser.Focus()
                        MsgBox("Username already exist!", MsgBoxStyle.Critical, "CourseWare")

                    End If

                Else
                    MsgBox("Password Did not Match Re-enter Passsword")
                    txtpass.Clear()
                    txtrepass.Clear()
                    txtpass.Focus()
                End If

            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub popDgvMs()

        da = New OleDbDataAdapter("SELECT * FROM tbl_user where access='stud'", conn)
        ds = New DataSet
        da.Fill(ds, "tbl_user")
        DataGridView1.DataSource = ds.Tables("tbl_user").DefaultView
        With DataGridView1
            .Columns(0).Visible = False
            .Columns(1).Visible = False
            .Columns(2).Visible = False
            .Columns(3).Visible = False
            .Columns(4).Width = 280
            .Columns(5).Width = 260




            .Columns(4).HeaderText = "Student Name"
            .Columns(5).HeaderText = "Course"

        End With



    End Sub
    Private Sub txtname_TextChanged(sender As Object, e As EventArgs) Handles txtname.TextChanged

    End Sub

    Private Sub UC_StudentManagement_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        popDgvMs()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        popDgvMs()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim row As DataGridViewRow = DataGridView1.CurrentRow
        txtuser.Text = row.Cells(1).Value.ToString
        txtpass.Text = row.Cells(2).Value.ToString
        txtrepass.Text = row.Cells(2).Value.ToString
        txtname.Text = row.Cells(4).Value.ToString()
        txtcourse.Text = row.Cells(5).Value.ToString()

        btnadd.Enabled = False
        btnedit.Enabled = True
        btndel.Enabled = True
        txtuser.Enabled = False
    End Sub

    Private Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        txtrepass.Clear()
        txtpass.Clear()
        txtuser.Clear()
        txtname.Clear()
        txtcourse.Clear()

        btnadd.Enabled = True
        btnedit.Enabled = False
        btndel.Enabled = False
        txtuser.Enabled = True
    End Sub

    Private Sub btnadd_Click(sender As Object, e As EventArgs) Handles btnadd.Click
        add()
    End Sub

    Private Sub btndel_Click(sender As Object, e As EventArgs) Handles btndel.Click
        del()
    End Sub

    Private Sub btnedit_Click(sender As Object, e As EventArgs) Handles btnedit.Click
        update12()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class
