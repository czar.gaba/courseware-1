﻿Imports System.IO
Imports System.Data.OleDb

Public Class course_edit
    Dim openFileDialog1 As New OpenFileDialog()

    Sub week1()
        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_syllabus Where week='" & lblweek.Text & "' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_syllabus")

            If (ds.Tables("tbl_syllabus").Rows.Count <> 0) Then

                txtcc.Text = ds.Tables("tbl_syllabus").Rows(0).Item("content")
                lblvid.Text = ds.Tables("tbl_syllabus").Rows(0).Item("video")
                lblpdf.Text = ds.Tables("tbl_syllabus").Rows(0).Item("file")
                lblpathpdf.Text = ds.Tables("tbl_syllabus").Rows(0).Item("file")
                lblpathvid.Text = ds.Tables("tbl_syllabus").Rows(0).Item("video")



            Else

            End If








        Catch ex As Exception
            txtcc.Clear()
            lblvid.Text = ""
            lblpdf.Text = ""
            lblpathpdf.Text = ""
            lblpathvid.Text = ""

        End Try

    End Sub


    Sub update_lecture()

        'Specify the directories you want to manipulate.
        Dim pathvid As String = lblvid.Text
        Dim path2 = "C:\courseware-1\WindowsApp1\Resources\" + lblweek.Text + ".mp4"
        Try
            My.Computer.FileSystem.CopyFile(pathvid,
                        path2,
                        FileIO.UIOption.AllDialogs,
                        FileIO.UICancelOption.ThrowException)
            lblpathvid.Text = path2

        Catch ex As Exception

        End Try

        ' Specify the directories you want to manipulate.
        Dim pathpdf As String = lblpdf.Text
        Dim path3 = "C:\courseware-1\WindowsApp1\Resources\" + lblweek.Text + ".pdf"
        Try
            My.Computer.FileSystem.CopyFile(pathpdf,
                        path3,
                        FileIO.UIOption.AllDialogs,
                        FileIO.UICancelOption.ThrowException)
            lblpathpdf.Text = path3

        Catch ex As Exception

        End Try


        Try
            connection()

            If txtcc.Text = "" And lblvid.Text = "" And lblpdf.Text = "" Then
                MsgBox("empty")
            Else




                Dim yesno As DialogResult = MessageBox.Show("confirm?", "update", MessageBoxButtons.OKCancel)

                If yesno = DialogResult.Cancel Then

                ElseIf yesno = DialogResult.OK Then
                    st = "UPDATE [tbl_syllabus] SET [content]='" & txtcc.Text & "', [video]='" & lblpathvid.Text & "', [file]='" & lblpathpdf.Text & "' WHERE [week]='" & lblweek.Text & "'"
                    cm = New OleDbCommand(st, module1.conn)

                    cm.ExecuteScalar()




                    MsgBox("updated")
                    dashboard_admin.UC_Courseoutline1.refresh.PerformClick()
                    Me.Close()


                End If



            End If



        Catch ex As Exception
            MsgBox(ex.Message)
        End Try






    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnpdf.Click

        'set the root to the z drive
        openFileDialog1.InitialDirectory = "C:\"
        'make sure the root goes back to where the user started
        openFileDialog1.RestoreDirectory = True

        'filter select files
        openFileDialog1.Filter = "PDF Files|*.pdf"

        'show the dialog
        openFileDialog1.ShowDialog()




        'check there is something to work with... the user did not exit before selecting a file etc.
        If openFileDialog1.FileName.Length = 0 Then

            'if the user selected a file set the value of the replacefile text box
        Else
            lblpdf.Text = System.IO.Path.GetFullPath(openFileDialog1.FileName)


        End If

    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)

        update_lecture()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'set the root to the z drive
        openFileDialog1.InitialDirectory = "C:\"
        'make sure the root goes back to where the user started
        openFileDialog1.RestoreDirectory = True

        'filter select files
        openFileDialog1.Filter = "All Media Files|*.mp4"

        'show the dialog
        openFileDialog1.ShowDialog()




        'check there is something to work with... the user did not exit before selecting a file etc.
        If openFileDialog1.FileName.Length = 0 Then

            'if the user selected a file set the value of the replacefile text box
        Else
            lblvid.Text = System.IO.Path.GetFullPath(openFileDialog1.FileName)


        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        update_lecture()
    End Sub

    Private Sub course_edit_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        week1()
    End Sub
End Class