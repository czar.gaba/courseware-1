﻿Imports System.Data.OleDb

Public Class frm_addquestions
    Dim sql As String
    Dim a As Integer = 1

    Sub addquiz()

        Try
            connection()

            da = New OleDbDataAdapter("SELECT * FROM tbl_questions where t_id='" & lblqid.Text & "'", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_questions")

            a = ds.Tables("tbl_questions").Rows.Count
            a = a + 1

            If cmbtype.Text = "True or False" Then

                sql = "Insert Into tbl_questions (`t_id`, `ques_id`, `ques_content`, `ques_answer`, `ques_type`, `choice_A`,`choice_B`,`choice_C`,`choice_D`)Values('" & lblqid.Text & "','" & a & "','" & txtquest.Text & "','" & txttorfanswer.Text & "','torf','" & txtchoice_a.Text & "','" & txtchoice_b.Text & "','" & txtchoice_c.Text & "' ,'" & txtchoice_d.Text & "')"

            ElseIf cmbtype.Text = "Identification" Then

                sql = "Insert Into tbl_questions (`t_id`, `ques_id`, `ques_content`, `ques_answer`, `ques_type`, `choice_A`,`choice_B`,`choice_C`,`choice_D`)Values('" & lblqid.Text & "','" & a & "','" & txtquest.Text & "','" & txtidentificationanswer.Text & "','identification','" & txtchoice_a.Text & "','" & txtchoice_b.Text & "','" & txtchoice_c.Text & "' ,'" & txtchoice_d.Text & "')"

            ElseIf cmbtype.Text = "Multiple Choice" Then

                sql = "Insert Into tbl_questions (`t_id`, `ques_id`, `ques_content`, `ques_answer`, `ques_type`, `choice_A`,`choice_B`,`choice_C`,`choice_D`)Values('" & lblqid.Text & "','" & a & "','" & txtquest.Text & "','" & txtmultianswer.Text & "','multiple','" & txtchoice_a.Text & "','" & txtchoice_b.Text & "','" & txtchoice_c.Text & "' ,'" & txtchoice_d.Text & "')"

            End If




            cm = New OleDbCommand(sql, module1.conn)
            Dim i As Integer
            i = cm.ExecuteNonQuery
            If i > 0 Then

                cmbtype.Text = ""
                txtquest.Clear()
                txtchoice_a.Clear()
                txtchoice_b.Clear()
                txtchoice_c.Clear()
                txtchoice_d.Clear()
                txttorfanswer.Text = ""
                txtmultianswer.Clear()
                txtidentificationanswer.Clear()



            Else
                cmbtype.Text = ""
                txtquest.Clear()
                txtchoice_a.Clear()
                txtchoice_b.Clear()
                txtchoice_c.Clear()
                txtchoice_d.Clear()
                txttorfanswer.Text = ""
                txtmultianswer.Clear()
                txtidentificationanswer.Clear()
                MsgBox("ERROR")

            End If












        Catch ex As Exception
            MsgBox("ERROR101")
        End Try
    End Sub
    Private Sub cmbtype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbtype.SelectedIndexChanged
        If cmbtype.Text = "True or False" Then
            torf.Visible = True
            identification.Visible = False
            multiple.Visible = False




        ElseIf cmbtype.Text = "Identification" Then
            torf.Visible = False
            identification.Visible = True
            multiple.Visible = False



        Else

            torf.Visible = False
            identification.Visible = False
            multiple.Visible = True



        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        frm_updelquestions.ShowDialog()

    End Sub

    Private Sub txtquest_TextChanged(sender As Object, e As EventArgs) Handles txtquest.TextChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If txtquest.Text <> "" Then

            If cmbtype.Text = "True or False" Then
                If txttorfanswer.Text <> "" Then
                    addquiz()
                Else
                    MsgBox("Complete the Following")
                End If
            ElseIf cmbtype.Text = "Identification" Then
                If txtidentificationanswer.Text <> "" Then
                    addquiz()
                Else
                    MsgBox("Complete the Following")
                End If
            ElseIf cmbtype.Text = "Multiple Choice" Then

                If txtmultianswer.Text <> "" Then
                    addquiz()
                Else
                    MsgBox("Complete the Following")
                End If
            End If
        Else
            MsgBox("Complete the Following")
        End If


    End Sub
End Class