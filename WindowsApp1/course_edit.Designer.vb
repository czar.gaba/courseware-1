﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class course_edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtcc = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnpdf = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblpdf = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.lblweek = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.lblvid = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.lblpathvid = New System.Windows.Forms.Label()
        Me.lblpathpdf = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtcc
        '
        Me.txtcc.Location = New System.Drawing.Point(64, 95)
        Me.txtcc.Name = "txtcc"
        Me.txtcc.Size = New System.Drawing.Size(434, 20)
        Me.txtcc.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 76)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Course Content"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(64, 139)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(10, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = " "
        '
        'btnpdf
        '
        Me.btnpdf.Location = New System.Drawing.Point(67, 180)
        Me.btnpdf.Name = "btnpdf"
        Me.btnpdf.Size = New System.Drawing.Size(75, 23)
        Me.btnpdf.TabIndex = 4
        Me.btnpdf.Text = "Browse"
        Me.btnpdf.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(64, 152)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Select File pdf"
        '
        'lblpdf
        '
        Me.lblpdf.AutoSize = True
        Me.lblpdf.Location = New System.Drawing.Point(159, 185)
        Me.lblpdf.Name = "lblpdf"
        Me.lblpdf.Size = New System.Drawing.Size(0, 13)
        Me.lblpdf.TabIndex = 7
        '
        'lblweek
        '
        Me.lblweek.AutoSize = True
        Me.lblweek.Location = New System.Drawing.Point(12, 19)
        Me.lblweek.Name = "lblweek"
        Me.lblweek.Size = New System.Drawing.Size(33, 13)
        Me.lblweek.TabIndex = 9
        Me.lblweek.Text = "week"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(67, 252)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 10
        Me.Button2.Text = "Browse"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'lblvid
        '
        Me.lblvid.AutoSize = True
        Me.lblvid.Location = New System.Drawing.Point(157, 257)
        Me.lblvid.Name = "lblvid"
        Me.lblvid.Size = New System.Drawing.Size(0, 13)
        Me.lblvid.TabIndex = 11
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(186, 442)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Save"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'lblpathvid
        '
        Me.lblpathvid.AutoSize = True
        Me.lblpathvid.Location = New System.Drawing.Point(1063, 548)
        Me.lblpathvid.Name = "lblpathvid"
        Me.lblpathvid.Size = New System.Drawing.Size(102, 13)
        Me.lblpathvid.TabIndex = 13
        Me.lblpathvid.Text = "dont delete just hide"
        Me.lblpathvid.Visible = False
        '
        'lblpathpdf
        '
        Me.lblpathpdf.AutoSize = True
        Me.lblpathpdf.Location = New System.Drawing.Point(861, 548)
        Me.lblpathpdf.Name = "lblpathpdf"
        Me.lblpathpdf.Size = New System.Drawing.Size(102, 13)
        Me.lblpathpdf.TabIndex = 14
        Me.lblpathpdf.Text = "dont delete just hide"
        Me.lblpathpdf.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(66, 236)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Select video file"
        '
        'course_edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1198, 570)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblpathpdf)
        Me.Controls.Add(Me.lblpathvid)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.lblvid)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.lblweek)
        Me.Controls.Add(Me.lblpdf)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnpdf)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtcc)
        Me.Name = "course_edit"
        Me.Text = " "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtcc As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnpdf As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents lblpdf As Label
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents lblweek As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents lblvid As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents lblpathvid As Label
    Friend WithEvents lblpathpdf As Label
    Friend WithEvents Label3 As Label
End Class
