﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UC_Grades
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(UC_Grades))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.txtsearch = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblfe = New System.Windows.Forms.Label()
        Me.lblme = New System.Windows.Forms.Label()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.lblfinal = New System.Windows.Forms.Label()
        Me.lblmidterm = New System.Windows.Forms.Label()
        Me.lblpe = New System.Windows.Forms.Label()
        Me.lblprelim = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lbla5 = New System.Windows.Forms.Label()
        Me.lbla4 = New System.Windows.Forms.Label()
        Me.lbla3 = New System.Windows.Forms.Label()
        Me.lbla2 = New System.Windows.Forms.Label()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.ass5 = New System.Windows.Forms.Label()
        Me.ass4 = New System.Windows.Forms.Label()
        Me.ass3 = New System.Windows.Forms.Label()
        Me.ass2 = New System.Windows.Forms.Label()
        Me.lbla1 = New System.Windows.Forms.Label()
        Me.ass1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblq5 = New System.Windows.Forms.Label()
        Me.lblq4 = New System.Windows.Forms.Label()
        Me.lblq3 = New System.Windows.Forms.Label()
        Me.lblq2 = New System.Windows.Forms.Label()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Quiz5 = New System.Windows.Forms.Label()
        Me.Quiz4 = New System.Windows.Forms.Label()
        Me.Quiz3 = New System.Windows.Forms.Label()
        Me.Quiz2 = New System.Windows.Forms.Label()
        Me.lblq1 = New System.Windows.Forms.Label()
        Me.Quiz1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblpercentgrade = New System.Windows.Forms.Label()
        Me.lblgrade = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblcourse = New System.Windows.Forms.Label()
        Me.lblname = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(40, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Grades"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(45, 84)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(446, 508)
        Me.DataGridView1.TabIndex = 0
        '
        'txtsearch
        '
        Me.txtsearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsearch.Location = New System.Drawing.Point(45, 640)
        Me.txtsearch.Name = "txtsearch"
        Me.txtsearch.Size = New System.Drawing.Size(270, 26)
        Me.txtsearch.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(40, 609)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(160, 25)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Search Student"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Bisque
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Location = New System.Drawing.Point(2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(910, 939)
        Me.Panel1.TabIndex = 3
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.lblfe)
        Me.GroupBox3.Controls.Add(Me.lblme)
        Me.GroupBox3.Controls.Add(Me.Panel16)
        Me.GroupBox3.Controls.Add(Me.Panel17)
        Me.GroupBox3.Controls.Add(Me.Panel18)
        Me.GroupBox3.Controls.Add(Me.lblfinal)
        Me.GroupBox3.Controls.Add(Me.lblmidterm)
        Me.GroupBox3.Controls.Add(Me.lblpe)
        Me.GroupBox3.Controls.Add(Me.lblprelim)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(40, 693)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(847, 208)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Exams"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(801, 138)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 29)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "%"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(801, 91)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 29)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "%"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(801, 43)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 29)
        Me.Label9.TabIndex = 24
        Me.Label9.Text = "%"
        '
        'lblfe
        '
        Me.lblfe.AutoSize = True
        Me.lblfe.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfe.Location = New System.Drawing.Point(750, 138)
        Me.lblfe.Name = "lblfe"
        Me.lblfe.Size = New System.Drawing.Size(39, 29)
        Me.lblfe.TabIndex = 18
        Me.lblfe.Text = "75"
        '
        'lblme
        '
        Me.lblme.AutoSize = True
        Me.lblme.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblme.Location = New System.Drawing.Point(750, 91)
        Me.lblme.Name = "lblme"
        Me.lblme.Size = New System.Drawing.Size(39, 29)
        Me.lblme.TabIndex = 17
        Me.lblme.Text = "75"
        '
        'Panel16
        '
        Me.Panel16.BackColor = System.Drawing.Color.Black
        Me.Panel16.Location = New System.Drawing.Point(191, 158)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(545, 2)
        Me.Panel16.TabIndex = 16
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.Black
        Me.Panel17.Location = New System.Drawing.Point(191, 112)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(545, 2)
        Me.Panel17.TabIndex = 16
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.Black
        Me.Panel18.Location = New System.Drawing.Point(191, 59)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(545, 2)
        Me.Panel18.TabIndex = 15
        '
        'lblfinal
        '
        Me.lblfinal.AutoSize = True
        Me.lblfinal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfinal.Location = New System.Drawing.Point(22, 138)
        Me.lblfinal.Name = "lblfinal"
        Me.lblfinal.Size = New System.Drawing.Size(132, 29)
        Me.lblfinal.TabIndex = 12
        Me.lblfinal.Text = "Final Exam"
        '
        'lblmidterm
        '
        Me.lblmidterm.AutoSize = True
        Me.lblmidterm.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblmidterm.Location = New System.Drawing.Point(22, 91)
        Me.lblmidterm.Name = "lblmidterm"
        Me.lblmidterm.Size = New System.Drawing.Size(167, 29)
        Me.lblmidterm.TabIndex = 11
        Me.lblmidterm.Text = "Midterm Exam"
        '
        'lblpe
        '
        Me.lblpe.AutoSize = True
        Me.lblpe.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpe.Location = New System.Drawing.Point(750, 42)
        Me.lblpe.Name = "lblpe"
        Me.lblpe.Size = New System.Drawing.Size(39, 29)
        Me.lblpe.TabIndex = 10
        Me.lblpe.Text = "75"
        '
        'lblprelim
        '
        Me.lblprelim.AutoSize = True
        Me.lblprelim.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblprelim.Location = New System.Drawing.Point(22, 42)
        Me.lblprelim.Name = "lblprelim"
        Me.lblprelim.Size = New System.Drawing.Size(149, 29)
        Me.lblprelim.TabIndex = 9
        Me.lblprelim.Text = "Prelim Exam"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.lbla5)
        Me.GroupBox2.Controls.Add(Me.lbla4)
        Me.GroupBox2.Controls.Add(Me.lbla3)
        Me.GroupBox2.Controls.Add(Me.lbla2)
        Me.GroupBox2.Controls.Add(Me.Panel9)
        Me.GroupBox2.Controls.Add(Me.Panel10)
        Me.GroupBox2.Controls.Add(Me.Panel11)
        Me.GroupBox2.Controls.Add(Me.Panel12)
        Me.GroupBox2.Controls.Add(Me.Panel13)
        Me.GroupBox2.Controls.Add(Me.ass5)
        Me.GroupBox2.Controls.Add(Me.ass4)
        Me.GroupBox2.Controls.Add(Me.ass3)
        Me.GroupBox2.Controls.Add(Me.ass2)
        Me.GroupBox2.Controls.Add(Me.lbla1)
        Me.GroupBox2.Controls.Add(Me.ass1)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(40, 374)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(847, 304)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Assignments"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(801, 234)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 29)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "%"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(801, 187)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 29)
        Me.Label13.TabIndex = 29
        Me.Label13.Text = "%"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(801, 138)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 29)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "%"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(801, 91)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 29)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "%"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(801, 43)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 29)
        Me.Label16.TabIndex = 26
        Me.Label16.Text = "%"
        '
        'lbla5
        '
        Me.lbla5.AutoSize = True
        Me.lbla5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbla5.Location = New System.Drawing.Point(750, 234)
        Me.lbla5.Name = "lbla5"
        Me.lbla5.Size = New System.Drawing.Size(39, 29)
        Me.lbla5.TabIndex = 20
        Me.lbla5.Text = "75"
        '
        'lbla4
        '
        Me.lbla4.AutoSize = True
        Me.lbla4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbla4.Location = New System.Drawing.Point(750, 183)
        Me.lbla4.Name = "lbla4"
        Me.lbla4.Size = New System.Drawing.Size(39, 29)
        Me.lbla4.TabIndex = 19
        Me.lbla4.Text = "75"
        '
        'lbla3
        '
        Me.lbla3.AutoSize = True
        Me.lbla3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbla3.Location = New System.Drawing.Point(750, 138)
        Me.lbla3.Name = "lbla3"
        Me.lbla3.Size = New System.Drawing.Size(39, 29)
        Me.lbla3.TabIndex = 18
        Me.lbla3.Text = "75"
        '
        'lbla2
        '
        Me.lbla2.AutoSize = True
        Me.lbla2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbla2.Location = New System.Drawing.Point(750, 91)
        Me.lbla2.Name = "lbla2"
        Me.lbla2.Size = New System.Drawing.Size(39, 29)
        Me.lbla2.TabIndex = 17
        Me.lbla2.Text = "75"
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Black
        Me.Panel9.Location = New System.Drawing.Point(191, 254)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(545, 2)
        Me.Panel9.TabIndex = 16
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.Black
        Me.Panel10.Location = New System.Drawing.Point(191, 201)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(545, 2)
        Me.Panel10.TabIndex = 16
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.Black
        Me.Panel11.Location = New System.Drawing.Point(191, 158)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(545, 2)
        Me.Panel11.TabIndex = 16
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.Black
        Me.Panel12.Location = New System.Drawing.Point(191, 112)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(545, 2)
        Me.Panel12.TabIndex = 16
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.Black
        Me.Panel13.Location = New System.Drawing.Point(191, 59)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(545, 2)
        Me.Panel13.TabIndex = 15
        '
        'ass5
        '
        Me.ass5.AutoSize = True
        Me.ass5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ass5.Location = New System.Drawing.Point(22, 234)
        Me.ass5.Name = "ass5"
        Me.ass5.Size = New System.Drawing.Size(157, 29)
        Me.ass5.TabIndex = 14
        Me.ass5.Text = "Assignment 5"
        '
        'ass4
        '
        Me.ass4.AutoSize = True
        Me.ass4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ass4.Location = New System.Drawing.Point(22, 183)
        Me.ass4.Name = "ass4"
        Me.ass4.Size = New System.Drawing.Size(157, 29)
        Me.ass4.TabIndex = 13
        Me.ass4.Text = "Assignment 4"
        '
        'ass3
        '
        Me.ass3.AutoSize = True
        Me.ass3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ass3.Location = New System.Drawing.Point(22, 138)
        Me.ass3.Name = "ass3"
        Me.ass3.Size = New System.Drawing.Size(157, 29)
        Me.ass3.TabIndex = 12
        Me.ass3.Text = "Assignment 3"
        '
        'ass2
        '
        Me.ass2.AutoSize = True
        Me.ass2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ass2.Location = New System.Drawing.Point(22, 91)
        Me.ass2.Name = "ass2"
        Me.ass2.Size = New System.Drawing.Size(157, 29)
        Me.ass2.TabIndex = 11
        Me.ass2.Text = "Assignment 2"
        '
        'lbla1
        '
        Me.lbla1.AutoSize = True
        Me.lbla1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbla1.Location = New System.Drawing.Point(750, 42)
        Me.lbla1.Name = "lbla1"
        Me.lbla1.Size = New System.Drawing.Size(39, 29)
        Me.lbla1.TabIndex = 10
        Me.lbla1.Text = "75"
        '
        'ass1
        '
        Me.ass1.AutoSize = True
        Me.ass1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ass1.Location = New System.Drawing.Point(22, 42)
        Me.ass1.Name = "ass1"
        Me.ass1.Size = New System.Drawing.Size(157, 29)
        Me.ass1.TabIndex = 9
        Me.ass1.Text = "Assignment 1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblq5)
        Me.GroupBox1.Controls.Add(Me.lblq4)
        Me.GroupBox1.Controls.Add(Me.lblq3)
        Me.GroupBox1.Controls.Add(Me.lblq2)
        Me.GroupBox1.Controls.Add(Me.Panel8)
        Me.GroupBox1.Controls.Add(Me.Panel7)
        Me.GroupBox1.Controls.Add(Me.Panel6)
        Me.GroupBox1.Controls.Add(Me.Panel5)
        Me.GroupBox1.Controls.Add(Me.Panel4)
        Me.GroupBox1.Controls.Add(Me.Quiz5)
        Me.GroupBox1.Controls.Add(Me.Quiz4)
        Me.GroupBox1.Controls.Add(Me.Quiz3)
        Me.GroupBox1.Controls.Add(Me.Quiz2)
        Me.GroupBox1.Controls.Add(Me.lblq1)
        Me.GroupBox1.Controls.Add(Me.Quiz1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(40, 50)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(847, 304)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Quizes"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(801, 234)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 29)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "%"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(801, 187)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 29)
        Me.Label11.TabIndex = 24
        Me.Label11.Text = "%"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(801, 138)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 29)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "%"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(801, 91)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 29)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "%"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(801, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 29)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "%"
        '
        'lblq5
        '
        Me.lblq5.AutoSize = True
        Me.lblq5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblq5.Location = New System.Drawing.Point(750, 234)
        Me.lblq5.Name = "lblq5"
        Me.lblq5.Size = New System.Drawing.Size(39, 29)
        Me.lblq5.TabIndex = 20
        Me.lblq5.Text = "75"
        '
        'lblq4
        '
        Me.lblq4.AutoSize = True
        Me.lblq4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblq4.Location = New System.Drawing.Point(750, 183)
        Me.lblq4.Name = "lblq4"
        Me.lblq4.Size = New System.Drawing.Size(39, 29)
        Me.lblq4.TabIndex = 19
        Me.lblq4.Text = "75"
        '
        'lblq3
        '
        Me.lblq3.AutoSize = True
        Me.lblq3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblq3.Location = New System.Drawing.Point(750, 138)
        Me.lblq3.Name = "lblq3"
        Me.lblq3.Size = New System.Drawing.Size(39, 29)
        Me.lblq3.TabIndex = 18
        Me.lblq3.Text = "75"
        '
        'lblq2
        '
        Me.lblq2.AutoSize = True
        Me.lblq2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblq2.Location = New System.Drawing.Point(750, 91)
        Me.lblq2.Name = "lblq2"
        Me.lblq2.Size = New System.Drawing.Size(39, 29)
        Me.lblq2.TabIndex = 17
        Me.lblq2.Text = "75"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Black
        Me.Panel8.Location = New System.Drawing.Point(126, 251)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(610, 2)
        Me.Panel8.TabIndex = 16
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Black
        Me.Panel7.Location = New System.Drawing.Point(126, 198)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(610, 2)
        Me.Panel7.TabIndex = 16
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Black
        Me.Panel6.Location = New System.Drawing.Point(126, 155)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(610, 2)
        Me.Panel6.TabIndex = 16
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Black
        Me.Panel5.Location = New System.Drawing.Point(126, 109)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(610, 2)
        Me.Panel5.TabIndex = 16
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Black
        Me.Panel4.Location = New System.Drawing.Point(126, 56)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(610, 2)
        Me.Panel4.TabIndex = 15
        '
        'Quiz5
        '
        Me.Quiz5.AutoSize = True
        Me.Quiz5.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Quiz5.Location = New System.Drawing.Point(22, 234)
        Me.Quiz5.Name = "Quiz5"
        Me.Quiz5.Size = New System.Drawing.Size(81, 29)
        Me.Quiz5.TabIndex = 14
        Me.Quiz5.Text = "Quiz 5"
        '
        'Quiz4
        '
        Me.Quiz4.AutoSize = True
        Me.Quiz4.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Quiz4.Location = New System.Drawing.Point(22, 183)
        Me.Quiz4.Name = "Quiz4"
        Me.Quiz4.Size = New System.Drawing.Size(81, 29)
        Me.Quiz4.TabIndex = 13
        Me.Quiz4.Text = "Quiz 4"
        '
        'Quiz3
        '
        Me.Quiz3.AutoSize = True
        Me.Quiz3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Quiz3.Location = New System.Drawing.Point(22, 138)
        Me.Quiz3.Name = "Quiz3"
        Me.Quiz3.Size = New System.Drawing.Size(81, 29)
        Me.Quiz3.TabIndex = 12
        Me.Quiz3.Text = "Quiz 3"
        '
        'Quiz2
        '
        Me.Quiz2.AutoSize = True
        Me.Quiz2.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Quiz2.Location = New System.Drawing.Point(22, 91)
        Me.Quiz2.Name = "Quiz2"
        Me.Quiz2.Size = New System.Drawing.Size(81, 29)
        Me.Quiz2.TabIndex = 11
        Me.Quiz2.Text = "Quiz 2"
        '
        'lblq1
        '
        Me.lblq1.AutoSize = True
        Me.lblq1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblq1.Location = New System.Drawing.Point(750, 42)
        Me.lblq1.Name = "lblq1"
        Me.lblq1.Size = New System.Drawing.Size(52, 29)
        Me.lblq1.TabIndex = 10
        Me.lblq1.Text = "100"
        '
        'Quiz1
        '
        Me.Quiz1.AutoSize = True
        Me.Quiz1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Quiz1.Location = New System.Drawing.Point(22, 42)
        Me.Quiz1.Name = "Quiz1"
        Me.Quiz1.Size = New System.Drawing.Size(81, 29)
        Me.Quiz1.TabIndex = 9
        Me.Quiz1.Text = "Quiz 1"
        '
        'Panel2
        '
        Me.Panel2.AutoScroll = True
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Location = New System.Drawing.Point(562, 250)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(936, 629)
        Me.Panel2.TabIndex = 4
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.Panel3.Controls.Add(Me.lblpercentgrade)
        Me.Panel3.Controls.Add(Me.lblgrade)
        Me.Panel3.Controls.Add(Me.Label24)
        Me.Panel3.Controls.Add(Me.Label3)
        Me.Panel3.Controls.Add(Me.lblcourse)
        Me.Panel3.Controls.Add(Me.lblname)
        Me.Panel3.Controls.Add(Me.PictureBox1)
        Me.Panel3.Location = New System.Drawing.Point(562, 84)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(936, 165)
        Me.Panel3.TabIndex = 5
        '
        'lblpercentgrade
        '
        Me.lblpercentgrade.AutoSize = True
        Me.lblpercentgrade.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpercentgrade.Location = New System.Drawing.Point(898, 25)
        Me.lblpercentgrade.Name = "lblpercentgrade"
        Me.lblpercentgrade.Size = New System.Drawing.Size(35, 29)
        Me.lblpercentgrade.TabIndex = 26
        Me.lblpercentgrade.Text = "%"
        '
        'lblgrade
        '
        Me.lblgrade.AutoSize = True
        Me.lblgrade.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblgrade.Location = New System.Drawing.Point(856, 25)
        Me.lblgrade.Name = "lblgrade"
        Me.lblgrade.Size = New System.Drawing.Size(39, 29)
        Me.lblgrade.TabIndex = 10
        Me.lblgrade.Text = "75"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(704, 25)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(151, 29)
        Me.Label24.TabIndex = 9
        Me.Label24.Text = "Final Grade :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(374, 129)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(176, 29)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Course Grades"
        '
        'lblcourse
        '
        Me.lblcourse.AutoSize = True
        Me.lblcourse.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcourse.Location = New System.Drawing.Point(127, 59)
        Me.lblcourse.Name = "lblcourse"
        Me.lblcourse.Size = New System.Drawing.Size(60, 20)
        Me.lblcourse.TabIndex = 7
        Me.lblcourse.Text = "Course"
        '
        'lblname
        '
        Me.lblname.AutoSize = True
        Me.lblname.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblname.Location = New System.Drawing.Point(125, 25)
        Me.lblname.Name = "lblname"
        Me.lblname.Size = New System.Drawing.Size(166, 29)
        Me.lblname.TabIndex = 6
        Me.lblname.Text = "Student Name"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(28, 13)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(91, 74)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(416, 55)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Refresh Data"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'UC_Grades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtsearch)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "UC_Grades"
        Me.Size = New System.Drawing.Size(1684, 988)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents txtsearch As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents lblfe As Label
    Friend WithEvents lblme As Label
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Panel18 As Panel
    Friend WithEvents lblfinal As Label
    Friend WithEvents lblmidterm As Label
    Friend WithEvents lblpe As Label
    Friend WithEvents lblprelim As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lbla5 As Label
    Friend WithEvents lbla4 As Label
    Friend WithEvents lbla3 As Label
    Friend WithEvents lbla2 As Label
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Panel10 As Panel
    Friend WithEvents Panel11 As Panel
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Panel13 As Panel
    Friend WithEvents ass5 As Label
    Friend WithEvents ass4 As Label
    Friend WithEvents ass3 As Label
    Friend WithEvents ass2 As Label
    Friend WithEvents lbla1 As Label
    Friend WithEvents ass1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblq5 As Label
    Friend WithEvents lblq4 As Label
    Friend WithEvents lblq3 As Label
    Friend WithEvents lblq2 As Label
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Quiz5 As Label
    Friend WithEvents Quiz4 As Label
    Friend WithEvents Quiz3 As Label
    Friend WithEvents Quiz2 As Label
    Friend WithEvents lblq1 As Label
    Friend WithEvents Quiz1 As Label
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents lblgrade As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblcourse As Label
    Friend WithEvents lblname As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblpercentgrade As Label
    Friend WithEvents Button1 As Button
End Class
