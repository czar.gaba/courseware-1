﻿Imports System.Data.OleDb

Public Class UC_Courseoutline
    Dim content As String
    Dim video As String
    Dim file As String

    Sub week1()
        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_syllabus Where week='week1' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_syllabus")

            If (ds.Tables("tbl_syllabus").Rows.Count <> 0) Then

                content = ds.Tables("tbl_syllabus").Rows(0).Item("content")
                video = ds.Tables("tbl_syllabus").Rows(0).Item("video")
                file = ds.Tables("tbl_syllabus").Rows(0).Item("file")


                frm_video.AxWindowsMediaPlayer1.URL = video
                frm_pdfreader.AxAcroPDF1.src = file
                lblcontent.Text = content

            Else
                MsgBox("No Record on Database")
            End If








        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub


    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        week1()
        frm_video.Show()


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        week1()
        frm_video.AxWindowsMediaPlayer1.close()
        frm_pdfreader.ShowDialog()
    End Sub

    Private Sub UC_Courseoutline_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        week1()
        frm_video.AxWindowsMediaPlayer1.close()
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        course_edit.lblweek.Text = "week1"
        course_edit.ShowDialog()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Dim yesno As DialogResult = MessageBox.Show("Attempt?", "confirm", MessageBoxButtons.OKCancel)

        If yesno = DialogResult.Cancel Then

        ElseIf yesno = DialogResult.OK Then
            frm_quiz.lblquiz.Text = "q1"
            frm_quiz.ShowDialog()

        End If

    End Sub

    Private Sub refresh_Click(sender As Object, e As EventArgs) Handles refresh.Click
        week1()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs)
        course_edit.lblweek.Text = "week2"
        course_edit.ShowDialog()
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs)
        course_edit.lblweek.Text = "week4"
        course_edit.ShowDialog()
    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        frm_addquestions.lblquiztitle.Text = "Quiz 1"
        frm_addquestions.lblqid.Text = "q1"
        frm_addquestions.ShowDialog()

    End Sub
End Class
