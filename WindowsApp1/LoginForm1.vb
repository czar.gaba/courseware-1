Imports System.Data.OleDb
Imports Tulpep.NotificationWindow
Public Class LoginForm1



    ' TODO: Insert code to perform custom authentication using the provided username and password 
    ' (See https://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        login()
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub LoginForm1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        connection()
    End Sub
    Sub login()

        Dim objpopup As New PopupNotifier
        objpopup.Image = My.Resources._202_2028426_buff_doge_hd_png_download.GetThumbnailImage(40, 40, Nothing, IntPtr.Zero)
        objpopup.TitleText = "Courseware"

        da = New OleDbDataAdapter("SELECT * FROM tbl_user WHERE username = '" & txtuser.Text & "' AND password = '" & txtpass.Text & "' ", module1.conn)
        ds = New DataSet
        da.Fill(ds, "tbl_user")
        If (ds.Tables("tbl_user").Rows.Count <> 0) Then
            objpopup.ContentText = "Welcome Back " + txtuser.Text
            objpopup.Popup()

            'passing value
            log(0) = ds.Tables("tbl_user").Rows(0).Item(0).ToString 'ID
            log(1) = ds.Tables("tbl_user").Rows(0).Item(1).ToString 'user
            log(2) = ds.Tables("tbl_user").Rows(0).Item(2).ToString 'pass
            log(3) = ds.Tables("tbl_user").Rows(0).Item(3).ToString 'access
            log(4) = ds.Tables("tbl_user").Rows(0).Item(4).ToString 'name
            log(5) = ds.Tables("tbl_user").Rows(0).Item(5).ToString 'course


            ac = ds.Tables("tbl_user").Rows(0).Item("access").ToString
            If ac = "admin" Then

                dashboard_admin.Tag = txtuser.Text
                dashboard_admin.Show()
                txtuser.Text = "Username"
                txtpass.Text = "Password"
                txtuser.Focus()
                Me.Hide()
            Else


                dashboard_students.Tag = txtuser.Text
                dashboard_students.Show()
                txtuser.Text = "Username"
                txtpass.Text = "Password"
                txtuser.Focus()
                MsgBox("student dash")
                Me.Hide()
            End If


        Else
            MsgBox("Username and Password is Invalid!", MsgBoxStyle.Critical, "Try Again!")
            txtuser.Text = "Username"
            txtpass.Text = "Password"
            txtuser.Focus()
        End If

    End Sub

    Private Sub txtpass_KeyDown(sender As Object, e As KeyEventArgs) Handles txtpass.KeyDown
        If Keys.KeyCode = Keys.Enter Then
            login()
        End If
    End Sub

    Private Sub txtuser_TextChanged(sender As Object, e As EventArgs) Handles txtuser.TextChanged

    End Sub

    Private Sub txtuser_GotFocus(sender As Object, e As EventArgs) Handles txtuser.GotFocus
        If txtuser.Text = "Username" Then
            txtuser.Clear()
            txtuser.ForeColor = Color.FromArgb(103, 58, 183)
            Panel1.BackColor = Color.FromArgb(103, 58, 183)


        End If
    End Sub

    Private Sub txtuser_LostFocus(sender As Object, e As EventArgs) Handles txtuser.LostFocus
        If txtuser.Text = "" Then
            txtuser.Text = "Username"
            txtuser.ForeColor = Color.Black
            Panel1.BackColor = Color.Black

        End If
    End Sub

    Private Sub txtpass_TextChanged(sender As Object, e As EventArgs) Handles txtpass.TextChanged

    End Sub

    Private Sub txtpass_LostFocus(sender As Object, e As EventArgs) Handles txtpass.LostFocus
        If txtpass.Text = "" Then
            txtpass.Text = "Password"

            txtpass.ForeColor = Color.Black
            Panel2.BackColor = Color.Black

        End If
    End Sub

    Private Sub txtpass_GotFocus(sender As Object, e As EventArgs) Handles txtpass.GotFocus
        If txtpass.Text = "Password" Then
            txtpass.Clear()
            txtpass.ForeColor = Color.FromArgb(103, 58, 183)
            Panel2.BackColor = Color.FromArgb(103, 58, 183)

        End If
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
        Close()
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        txtpass.PasswordChar = ""
        PictureBox3.Visible = True
        PictureBox2.Visible = False
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        txtpass.PasswordChar = "�"
        PictureBox3.Visible = False
        PictureBox2.Visible = True
    End Sub
End Class
