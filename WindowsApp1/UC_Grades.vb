﻿Imports System.Data.OleDb
Public Class UC_Grades
    Sub compute()
        Dim q1 As Double = lblq1.Text
        Dim q2 As Double = lblq2.Text
        Dim q3 As Double = lblq3.Text
        Dim q4 As Double = lblq4.Text
        Dim q5 As Double = lblq5.Text


        Dim a1 As Double = lbla1.Text
        Dim a2 As Double = lbla2.Text
        Dim a3 As Double = lbla3.Text
        Dim a4 As Double = lbla4.Text
        Dim a5 As Double = lbla5.Text




        a1 = a1 * 0.3
        a2 = a2 * 0.3
        a3 = a3 * 0.3
        a4 = a4 * 0.3
        a5 = a5 * 0.3




        q1 = q1 * 0.3
        q2 = q2 * 0.3
        q3 = q3 * 0.3
        q4 = q4 * 0.3
        q5 = q5 * 0.3

        If lblpe.Text = "N/A" Or lblme.Text = "N/A" Or lblfe.Text = "N/A" Then
            Dim pe As String = lblpe.Text
            Dim mex As String = lblme.Text
            Dim fe As String = lblfe.Text

            lblgrade.Text = "N/A"
            lblpercentgrade.Visible = False


        Else

            Dim pe As Double = lblpe.Text
            Dim mex As Double = lblme.Text
            Dim fe As Double = lblfe.Text

            lblpercentgrade.Visible = True
        End If



    End Sub

    Sub getexamgrades()

        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_grading Where type='Exam' and student_name='" & lblname.Text & "' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_grading")

            Dim q_title As String


            Dim a As Integer = 0
            For Each Row As DataRow In ds.Tables(0).Rows

                q_title = ds.Tables("tbl_grading").Rows(a).Item("quiz")

                If q_title = lblprelim.Text Then
                    lblpe.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                    If lblpe.Text = "N/A%" Then
                        lblpe.Text = "N/A"
                    End If
                ElseIf q_title = lblmidterm.Text Then
                    lblme.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                    If lblme.Text = "N/A%" Then
                        lblme.Text = "N/A"
                    End If
                ElseIf q_title = lblfinal.Text Then
                    lblfe.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                    If lblfe.Text = "N/A%" Then
                        lblfe.Text = "N/A"
                    End If

                Else

                End If


                a = a + 1
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Sub getassgrades()

        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_grading Where type='Assignment' and student_name='" & lblname.Text & "' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_grading")

            Dim q_title As String


            Dim a As Integer = 0
            For Each Row As DataRow In ds.Tables(0).Rows

                q_title = ds.Tables("tbl_grading").Rows(a).Item("quiz")

                If q_title = ass1.Text Then
                    lbla1.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = ass2.Text Then
                    lbla2.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = ass3.Text Then
                    lbla3.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = ass4.Text Then
                    lbla4.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = ass5.Text Then
                    lbla5.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                Else

                End If


                a = a + 1
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Sub getquizgrades()

        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_grading Where type='Quiz' and student_name='" & lblname.Text & "' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_grading")

            Dim q_title As String


            Dim a As Integer = 0
            For Each Row As DataRow In ds.Tables(0).Rows

                q_title = ds.Tables("tbl_grading").Rows(a).Item("quiz")

                If q_title = Quiz1.Text Then
                    lblq1.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = Quiz2.Text Then
                    lblq2.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = Quiz3.Text Then
                    lblq3.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = Quiz4.Text Then
                    lblq4.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                ElseIf q_title = Quiz5.Text Then
                    lblq5.Text = ds.Tables("tbl_grading").Rows(a).Item("grade")
                Else

                End If


                a = a + 1
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Sub search()
        Try

            da = New OleDbDataAdapter("SELECT * FROM tbl_user Where access='stud' and  name Like '%" & txtsearch.Text & "%'", module1.conn)


            ds = New DataSet
            da.Fill(ds, "tbl_user")

            If (ds.Tables("tbl_user").Rows.Count <> 0) Then

                DataGridView1.DataSource = ds.Tables("tbl_user").DefaultView
                With DataGridView1
                    .Columns(0).Visible = False
                    .Columns(1).Visible = False
                    .Columns(2).Visible = False
                    .Columns(3).Visible = False
                    .Columns(4).Width = 250
                    .Columns(5).Width = 150


                    .Columns(4).HeaderText = "Student Name"
                    .Columns(5).HeaderText = "Course"


                End With
            Else
                DataGridView1.DataSource = ds.Tables("tbl_user").DefaultView
                With DataGridView1
                    .Columns(0).Visible = False
                    .Columns(1).Visible = False
                    .Columns(2).Visible = False
                    .Columns(3).Visible = False
                    .Columns(4).Width = 250
                    .Columns(5).Width = 150


                    .Columns(4).HeaderText = "Student Name"
                    .Columns(5).HeaderText = "Course"

                End With
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Sub popDgvMs()
        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_user where access='stud'", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_user")
            DataGridView1.DataSource = ds.Tables("tbl_user").DefaultView
            With DataGridView1
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Visible = False
                .Columns(4).Width = 250
                .Columns(5).Width = 150


                .Columns(4).HeaderText = "Student Name"
                .Columns(5).HeaderText = "Course"

            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub UC_Grades_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        popDgvMs()

    End Sub

    Private Sub txtsearch_TextChanged(sender As Object, e As EventArgs) Handles txtsearch.TextChanged
        search()
    End Sub


    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick

        Dim row As DataGridViewRow = DataGridView1.CurrentRow
        lblname.Text = row.Cells(4).Value.ToString()
        lblcourse.Text = row.Cells(5).Value.ToString()
        getquizgrades()
        getassgrades()
        getexamgrades()

        compute()

    End Sub



    Private Sub lblname_TextChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        popDgvMs()
    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class
