﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_addquestions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblquiztitle = New System.Windows.Forms.Label()
        Me.lblqid = New System.Windows.Forms.Label()
        Me.txtquest = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cmbtype = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.multiple = New System.Windows.Forms.GroupBox()
        Me.txtmultianswer = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtchoice_d = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtchoice_c = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtchoice_b = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtchoice_a = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.torf = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txttorfanswer = New System.Windows.Forms.ComboBox()
        Me.identification = New System.Windows.Forms.GroupBox()
        Me.txtidentificationanswer = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.multiple.SuspendLayout()
        Me.torf.SuspendLayout()
        Me.identification.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblquiztitle
        '
        Me.lblquiztitle.AutoSize = True
        Me.lblquiztitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblquiztitle.Location = New System.Drawing.Point(684, 62)
        Me.lblquiztitle.Name = "lblquiztitle"
        Me.lblquiztitle.Size = New System.Drawing.Size(81, 24)
        Me.lblquiztitle.TabIndex = 0
        Me.lblquiztitle.Text = "Quiz title"
        Me.lblquiztitle.Visible = False
        '
        'lblqid
        '
        Me.lblqid.AutoSize = True
        Me.lblqid.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblqid.Location = New System.Drawing.Point(1281, 62)
        Me.lblqid.Name = "lblqid"
        Me.lblqid.Size = New System.Drawing.Size(66, 24)
        Me.lblqid.TabIndex = 1
        Me.lblqid.Text = "QuizID"
        Me.lblqid.Visible = False
        '
        'txtquest
        '
        Me.txtquest.Location = New System.Drawing.Point(65, 308)
        Me.txtquest.Multiline = True
        Me.txtquest.Name = "txtquest"
        Me.txtquest.Size = New System.Drawing.Size(337, 192)
        Me.txtquest.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 191)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(122, 24)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "question type"
        '
        'cmbtype
        '
        Me.cmbtype.FormattingEnabled = True
        Me.cmbtype.Items.AddRange(New Object() {"True or False", "Identification", "Multiple Choice"})
        Me.cmbtype.Location = New System.Drawing.Point(65, 219)
        Me.cmbtype.Name = "cmbtype"
        Me.cmbtype.Size = New System.Drawing.Size(233, 21)
        Me.cmbtype.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(61, 281)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 24)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Question"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(699, 191)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 24)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Choices"
        Me.Label3.Visible = False
        '
        'multiple
        '
        Me.multiple.Controls.Add(Me.txtmultianswer)
        Me.multiple.Controls.Add(Me.Label8)
        Me.multiple.Controls.Add(Me.txtchoice_d)
        Me.multiple.Controls.Add(Me.Label7)
        Me.multiple.Controls.Add(Me.txtchoice_c)
        Me.multiple.Controls.Add(Me.Label6)
        Me.multiple.Controls.Add(Me.txtchoice_b)
        Me.multiple.Controls.Add(Me.Label5)
        Me.multiple.Controls.Add(Me.txtchoice_a)
        Me.multiple.Controls.Add(Me.Label4)
        Me.multiple.Location = New System.Drawing.Point(703, 240)
        Me.multiple.Name = "multiple"
        Me.multiple.Size = New System.Drawing.Size(350, 346)
        Me.multiple.TabIndex = 7
        Me.multiple.TabStop = False
        Me.multiple.Text = "multiple"
        Me.multiple.Visible = False
        '
        'txtmultianswer
        '
        Me.txtmultianswer.Location = New System.Drawing.Point(26, 293)
        Me.txtmultianswer.Name = "txtmultianswer"
        Me.txtmultianswer.Size = New System.Drawing.Size(280, 20)
        Me.txtmultianswer.TabIndex = 18
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(22, 252)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 24)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Input Answer"
        '
        'txtchoice_d
        '
        Me.txtchoice_d.Location = New System.Drawing.Point(61, 200)
        Me.txtchoice_d.Name = "txtchoice_d"
        Me.txtchoice_d.Size = New System.Drawing.Size(227, 20)
        Me.txtchoice_d.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(22, 196)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(23, 24)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "D"
        '
        'txtchoice_c
        '
        Me.txtchoice_c.Location = New System.Drawing.Point(61, 150)
        Me.txtchoice_c.Name = "txtchoice_c"
        Me.txtchoice_c.Size = New System.Drawing.Size(227, 20)
        Me.txtchoice_c.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(22, 146)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(23, 24)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "C"
        '
        'txtchoice_b
        '
        Me.txtchoice_b.Location = New System.Drawing.Point(61, 100)
        Me.txtchoice_b.Name = "txtchoice_b"
        Me.txtchoice_b.Size = New System.Drawing.Size(227, 20)
        Me.txtchoice_b.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(22, 96)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(22, 24)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "B"
        '
        'txtchoice_a
        '
        Me.txtchoice_a.Location = New System.Drawing.Point(61, 46)
        Me.txtchoice_a.Name = "txtchoice_a"
        Me.txtchoice_a.Size = New System.Drawing.Size(227, 20)
        Me.txtchoice_a.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(23, 24)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "A"
        '
        'torf
        '
        Me.torf.Controls.Add(Me.Label9)
        Me.torf.Controls.Add(Me.txttorfanswer)
        Me.torf.Location = New System.Drawing.Point(1065, 240)
        Me.torf.Name = "torf"
        Me.torf.Size = New System.Drawing.Size(350, 138)
        Me.torf.TabIndex = 16
        Me.torf.TabStop = False
        Me.torf.Text = "torf"
        Me.torf.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(19, 41)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(131, 24)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Select Answer"
        '
        'txttorfanswer
        '
        Me.txttorfanswer.FormattingEnabled = True
        Me.txttorfanswer.Items.AddRange(New Object() {"True", "False"})
        Me.txttorfanswer.Location = New System.Drawing.Point(50, 79)
        Me.txttorfanswer.Name = "txttorfanswer"
        Me.txttorfanswer.Size = New System.Drawing.Size(233, 21)
        Me.txttorfanswer.TabIndex = 10
        '
        'identification
        '
        Me.identification.Controls.Add(Me.txtidentificationanswer)
        Me.identification.Controls.Add(Me.Label10)
        Me.identification.Location = New System.Drawing.Point(1065, 390)
        Me.identification.Name = "identification"
        Me.identification.Size = New System.Drawing.Size(350, 196)
        Me.identification.TabIndex = 20
        Me.identification.TabStop = False
        Me.identification.Text = "identification"
        Me.identification.Visible = False
        '
        'txtidentificationanswer
        '
        Me.txtidentificationanswer.Location = New System.Drawing.Point(22, 96)
        Me.txtidentificationanswer.Name = "txtidentificationanswer"
        Me.txtidentificationanswer.Size = New System.Drawing.Size(280, 20)
        Me.txtidentificationanswer.TabIndex = 20
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(18, 55)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(120, 24)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Input Answer"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(398, 657)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(290, 79)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Submit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(729, 657)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(290, 79)
        Me.Button2.TabIndex = 21
        Me.Button2.Text = "View Questions"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'frm_addquestions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1427, 801)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.identification)
        Me.Controls.Add(Me.torf)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.multiple)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cmbtype)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtquest)
        Me.Controls.Add(Me.lblqid)
        Me.Controls.Add(Me.lblquiztitle)
        Me.Name = "frm_addquestions"
        Me.Text = "frm_addquiz"
        Me.multiple.ResumeLayout(False)
        Me.multiple.PerformLayout()
        Me.torf.ResumeLayout(False)
        Me.torf.PerformLayout()
        Me.identification.ResumeLayout(False)
        Me.identification.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblquiztitle As Label
    Friend WithEvents lblqid As Label
    Friend WithEvents txtquest As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cmbtype As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents multiple As GroupBox
    Friend WithEvents txtchoice_d As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents txtchoice_c As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtchoice_b As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtchoice_a As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents torf As GroupBox
    Friend WithEvents txttorfanswer As ComboBox
    Friend WithEvents txtmultianswer As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents identification As GroupBox
    Friend WithEvents txtidentificationanswer As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class
