﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_quiz
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblquiz = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblquestion = New System.Windows.Forms.Label()
        Me.rba = New System.Windows.Forms.RadioButton()
        Me.rbb = New System.Windows.Forms.RadioButton()
        Me.rbc = New System.Windows.Forms.RadioButton()
        Me.rbd = New System.Windows.Forms.RadioButton()
        Me.lblquesid = New System.Windows.Forms.Label()
        Me.lblanswer = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.groupmulti = New System.Windows.Forms.GroupBox()
        Me.grouptof = New System.Windows.Forms.GroupBox()
        Me.rbtrue = New System.Windows.Forms.RadioButton()
        Me.rbfalse = New System.Windows.Forms.RadioButton()
        Me.groupfill = New System.Windows.Forms.GroupBox()
        Me.txtfill = New System.Windows.Forms.TextBox()
        Me.lblquestype = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblright = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblwrong = New System.Windows.Forms.Label()
        Me.groupmulti.SuspendLayout()
        Me.grouptof.SuspendLayout()
        Me.groupfill.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblquiz
        '
        Me.lblquiz.AutoSize = True
        Me.lblquiz.Location = New System.Drawing.Point(12, 9)
        Me.lblquiz.Name = "lblquiz"
        Me.lblquiz.Size = New System.Drawing.Size(79, 13)
        Me.lblquiz.TabIndex = 0
        Me.lblquiz.Text = "dont delete this"
        Me.lblquiz.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Location = New System.Drawing.Point(704, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(125, 100)
        Me.Panel1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 84)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(73, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Question"
        '
        'lblquestion
        '
        Me.lblquestion.AutoSize = True
        Me.lblquestion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblquestion.Location = New System.Drawing.Point(21, 118)
        Me.lblquestion.Name = "lblquestion"
        Me.lblquestion.Size = New System.Drawing.Size(73, 20)
        Me.lblquestion.TabIndex = 4
        Me.lblquestion.Text = "Question"
        '
        'rba
        '
        Me.rba.AutoSize = True
        Me.rba.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rba.Location = New System.Drawing.Point(33, 40)
        Me.rba.Name = "rba"
        Me.rba.Size = New System.Drawing.Size(126, 24)
        Me.rba.TabIndex = 5
        Me.rba.TabStop = True
        Me.rba.Text = "RadioButton1"
        Me.rba.UseVisualStyleBackColor = True
        '
        'rbb
        '
        Me.rbb.AutoSize = True
        Me.rbb.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbb.Location = New System.Drawing.Point(33, 86)
        Me.rbb.Name = "rbb"
        Me.rbb.Size = New System.Drawing.Size(126, 24)
        Me.rbb.TabIndex = 6
        Me.rbb.TabStop = True
        Me.rbb.Text = "RadioButton2"
        Me.rbb.UseVisualStyleBackColor = True
        '
        'rbc
        '
        Me.rbc.AutoSize = True
        Me.rbc.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbc.Location = New System.Drawing.Point(235, 40)
        Me.rbc.Name = "rbc"
        Me.rbc.Size = New System.Drawing.Size(126, 24)
        Me.rbc.TabIndex = 7
        Me.rbc.TabStop = True
        Me.rbc.Text = "RadioButton3"
        Me.rbc.UseVisualStyleBackColor = True
        '
        'rbd
        '
        Me.rbd.AutoSize = True
        Me.rbd.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbd.Location = New System.Drawing.Point(235, 86)
        Me.rbd.Name = "rbd"
        Me.rbd.Size = New System.Drawing.Size(126, 24)
        Me.rbd.TabIndex = 8
        Me.rbd.TabStop = True
        Me.rbd.Text = "RadioButton4"
        Me.rbd.UseVisualStyleBackColor = True
        '
        'lblquesid
        '
        Me.lblquesid.AutoSize = True
        Me.lblquesid.Location = New System.Drawing.Point(147, 12)
        Me.lblquesid.Name = "lblquesid"
        Me.lblquesid.Size = New System.Drawing.Size(38, 13)
        Me.lblquesid.TabIndex = 9
        Me.lblquesid.Text = "quesid"
        Me.lblquesid.Visible = False
        '
        'lblanswer
        '
        Me.lblanswer.AutoSize = True
        Me.lblanswer.Location = New System.Drawing.Point(254, 12)
        Me.lblanswer.Name = "lblanswer"
        Me.lblanswer.Size = New System.Drawing.Size(38, 13)
        Me.lblanswer.TabIndex = 10
        Me.lblanswer.Text = "quesid"
        Me.lblanswer.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(7, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(20, 20)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "A"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 90)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(20, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "B"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(209, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(20, 20)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "C"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(208, 86)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(21, 20)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "D"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(150, 333)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(133, 44)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = "Next"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'groupmulti
        '
        Me.groupmulti.Controls.Add(Me.rba)
        Me.groupmulti.Controls.Add(Me.rbb)
        Me.groupmulti.Controls.Add(Me.rbc)
        Me.groupmulti.Controls.Add(Me.Label6)
        Me.groupmulti.Controls.Add(Me.rbd)
        Me.groupmulti.Controls.Add(Me.Label5)
        Me.groupmulti.Controls.Add(Me.Label3)
        Me.groupmulti.Controls.Add(Me.Label4)
        Me.groupmulti.Location = New System.Drawing.Point(37, 185)
        Me.groupmulti.Name = "groupmulti"
        Me.groupmulti.Size = New System.Drawing.Size(367, 142)
        Me.groupmulti.TabIndex = 18
        Me.groupmulti.TabStop = False
        Me.groupmulti.Text = "Multiple"
        '
        'grouptof
        '
        Me.grouptof.Controls.Add(Me.rbtrue)
        Me.grouptof.Controls.Add(Me.rbfalse)
        Me.grouptof.Location = New System.Drawing.Point(70, 418)
        Me.grouptof.Name = "grouptof"
        Me.grouptof.Size = New System.Drawing.Size(183, 142)
        Me.grouptof.TabIndex = 19
        Me.grouptof.TabStop = False
        Me.grouptof.Text = "True or False"
        '
        'rbtrue
        '
        Me.rbtrue.AutoSize = True
        Me.rbtrue.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtrue.Location = New System.Drawing.Point(21, 40)
        Me.rbtrue.Name = "rbtrue"
        Me.rbtrue.Size = New System.Drawing.Size(59, 24)
        Me.rbtrue.TabIndex = 5
        Me.rbtrue.TabStop = True
        Me.rbtrue.Text = "True"
        Me.rbtrue.UseVisualStyleBackColor = True
        '
        'rbfalse
        '
        Me.rbfalse.AutoSize = True
        Me.rbfalse.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbfalse.Location = New System.Drawing.Point(21, 86)
        Me.rbfalse.Name = "rbfalse"
        Me.rbfalse.Size = New System.Drawing.Size(66, 24)
        Me.rbfalse.TabIndex = 6
        Me.rbfalse.TabStop = True
        Me.rbfalse.Text = "False"
        Me.rbfalse.UseVisualStyleBackColor = True
        '
        'groupfill
        '
        Me.groupfill.Controls.Add(Me.txtfill)
        Me.groupfill.Location = New System.Drawing.Point(329, 430)
        Me.groupfill.Name = "groupfill"
        Me.groupfill.Size = New System.Drawing.Size(412, 142)
        Me.groupfill.TabIndex = 20
        Me.groupfill.TabStop = False
        Me.groupfill.Text = "Fill in the Blank"
        '
        'txtfill
        '
        Me.txtfill.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfill.Location = New System.Drawing.Point(30, 58)
        Me.txtfill.Name = "txtfill"
        Me.txtfill.Size = New System.Drawing.Size(345, 26)
        Me.txtfill.TabIndex = 0
        '
        'lblquestype
        '
        Me.lblquestype.AutoSize = True
        Me.lblquestype.Location = New System.Drawing.Point(326, 12)
        Me.lblquestype.Name = "lblquestype"
        Me.lblquestype.Size = New System.Drawing.Size(50, 13)
        Me.lblquestype.TabIndex = 21
        Me.lblquestype.Text = "questype"
        Me.lblquestype.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(754, 12)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 22
        Me.Button4.Text = "done"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(526, 625)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "correct counter"
        Me.Label2.Visible = False
        '
        'lblright
        '
        Me.lblright.AutoSize = True
        Me.lblright.Location = New System.Drawing.Point(611, 625)
        Me.lblright.Name = "lblright"
        Me.lblright.Size = New System.Drawing.Size(13, 13)
        Me.lblright.TabIndex = 24
        Me.lblright.Text = "0"
        Me.lblright.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(13, 625)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "wrong counter"
        Me.Label8.Visible = False
        '
        'lblwrong
        '
        Me.lblwrong.AutoSize = True
        Me.lblwrong.Location = New System.Drawing.Point(94, 625)
        Me.lblwrong.Name = "lblwrong"
        Me.lblwrong.Size = New System.Drawing.Size(13, 13)
        Me.lblwrong.TabIndex = 26
        Me.lblwrong.Text = "0"
        Me.lblwrong.Visible = False
        '
        'frm_quiz
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 647)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblwrong)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblright)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.lblquestype)
        Me.Controls.Add(Me.groupfill)
        Me.Controls.Add(Me.grouptof)
        Me.Controls.Add(Me.groupmulti)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.lblanswer)
        Me.Controls.Add(Me.lblquesid)
        Me.Controls.Add(Me.lblquestion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblquiz)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frm_quiz"
        Me.Text = "frm_quiz"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.groupmulti.ResumeLayout(False)
        Me.groupmulti.PerformLayout()
        Me.grouptof.ResumeLayout(False)
        Me.grouptof.PerformLayout()
        Me.groupfill.ResumeLayout(False)
        Me.groupfill.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblquiz As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents lblquestion As Label
    Friend WithEvents rba As RadioButton
    Friend WithEvents rbb As RadioButton
    Friend WithEvents rbc As RadioButton
    Friend WithEvents rbd As RadioButton
    Friend WithEvents lblquesid As Label
    Friend WithEvents lblanswer As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents groupmulti As GroupBox
    Friend WithEvents grouptof As GroupBox
    Friend WithEvents rbtrue As RadioButton
    Friend WithEvents rbfalse As RadioButton
    Friend WithEvents groupfill As GroupBox
    Friend WithEvents txtfill As TextBox
    Friend WithEvents lblquestype As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents lblright As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents lblwrong As Label
End Class
