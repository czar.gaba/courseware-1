﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UC_profile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtuser = New System.Windows.Forms.TextBox()
        Me.txtpass1 = New System.Windows.Forms.TextBox()
        Me.txtname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btn_update = New System.Windows.Forms.Button()
        Me.txtcourse = New System.Windows.Forms.TextBox()
        Me.lblcourse = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtpass2 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'txtuser
        '
        Me.txtuser.Location = New System.Drawing.Point(180, 88)
        Me.txtuser.Name = "txtuser"
        Me.txtuser.Size = New System.Drawing.Size(236, 20)
        Me.txtuser.TabIndex = 0
        '
        'txtpass1
        '
        Me.txtpass1.Location = New System.Drawing.Point(180, 114)
        Me.txtpass1.Name = "txtpass1"
        Me.txtpass1.Size = New System.Drawing.Size(236, 20)
        Me.txtpass1.TabIndex = 1
        '
        'txtname
        '
        Me.txtname.Location = New System.Drawing.Point(180, 24)
        Me.txtname.Name = "txtname"
        Me.txtname.Size = New System.Drawing.Size(236, 20)
        Me.txtname.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(29, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "username"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(29, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "password"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(29, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "name"
        '
        'btn_update
        '
        Me.btn_update.Location = New System.Drawing.Point(341, 166)
        Me.btn_update.Name = "btn_update"
        Me.btn_update.Size = New System.Drawing.Size(75, 23)
        Me.btn_update.TabIndex = 6
        Me.btn_update.Text = "update"
        Me.btn_update.UseVisualStyleBackColor = True
        '
        'txtcourse
        '
        Me.txtcourse.Location = New System.Drawing.Point(180, 50)
        Me.txtcourse.Name = "txtcourse"
        Me.txtcourse.Size = New System.Drawing.Size(236, 20)
        Me.txtcourse.TabIndex = 7
        '
        'lblcourse
        '
        Me.lblcourse.AutoSize = True
        Me.lblcourse.Location = New System.Drawing.Point(29, 53)
        Me.lblcourse.Name = "lblcourse"
        Me.lblcourse.Size = New System.Drawing.Size(40, 13)
        Me.lblcourse.TabIndex = 8
        Me.lblcourse.Text = "Course"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(29, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "confirm password"
        '
        'txtpass2
        '
        Me.txtpass2.Location = New System.Drawing.Point(180, 140)
        Me.txtpass2.Name = "txtpass2"
        Me.txtpass2.Size = New System.Drawing.Size(236, 20)
        Me.txtpass2.TabIndex = 9
        '
        'UC_profile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 304)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtpass2)
        Me.Controls.Add(Me.lblcourse)
        Me.Controls.Add(Me.txtcourse)
        Me.Controls.Add(Me.btn_update)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtname)
        Me.Controls.Add(Me.txtpass1)
        Me.Controls.Add(Me.txtuser)
        Me.Name = "UC_profile"
        Me.Text = "UC_profile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtuser As TextBox
    Friend WithEvents txtpass1 As TextBox
    Friend WithEvents txtname As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btn_update As Button
    Friend WithEvents txtcourse As TextBox
    Friend WithEvents lblcourse As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtpass2 As TextBox
End Class
