﻿Imports System.Data.OleDb
Public Class frm_updelquestions
    Dim sql As String

    Sub del()
        Try

            connection()
            sql = ("DELETE FROM tbl_questions WHERE t_id = '" & txttid.Text & "' and ques_id = '" & txtques_id.Text & "'")
            cm = New OleDbCommand(sql, module1.conn)
            Dim i As Integer
            i = cm.ExecuteNonQuery

            If i > 0 Then

                MsgBox("Deleted!", MsgBoxStyle.Critical)
                popDgvquestions()
                txttid.Clear()
                txtques_id.Clear()
                txtquestype.Clear()
                txtquest.Clear()
                txtquesanswer.Clear()
                txta.Clear()
                txtb.Clear()
                txtc.Clear()
                txtd.Clear()


            Else
                MsgBox("Error on deleting data!")
            End If

        Catch ex As Exception

        End Try
    End Sub
    Sub update123123()
        Try


            connection()



            sql = "Update tbl_questions set `ques_content`='" & txtquest.Text & "', `ques_answer`='" & txtquesanswer.Text & "', `choice_A`='" & txta.Text & "' , `choice_B`='" & txtb.Text & "' , `choice_C`='" & txtc.Text & "' , `choice_D`='" & txtd.Text & "'WHERE t_id='" & txttid.Text & "' and ques_id='" & txtques_id.Text & "' "
                cm = New OleDbCommand(sql, module1.conn)
            Dim i As Integer
            i = cm.ExecuteNonQuery

            If i > 0 Then
                txttid.Clear()
                txtques_id.Clear()
                txtquestype.Clear()
                txtquest.Clear()
                txtquesanswer.Clear()
                txta.Clear()
                txtb.Clear()
                txtc.Clear()
                txtd.Clear()
                MsgBox("Update Successfully")
                popDgvquestions()

            End If
        Catch ex As Exception
            txttid.Clear()
            txtques_id.Clear()
            txtquestype.Clear()
            txtquest.Clear()
            txtquesanswer.Clear()
            txta.Clear()
            txtb.Clear()
            txtc.Clear()
            txtd.Clear()

        End Try


    End Sub
    Sub popDgvquestions()
        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_questions where t_id='" & frm_addquestions.lblqid.Text & "'", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_questions")
            DataGridView1.DataSource = ds.Tables("tbl_questions").DefaultView
            With DataGridView1
                .Columns(0).Visible = False
                .Columns(1).Visible = False
                .Columns(2).Visible = False
                .Columns(3).Width = 500
                .Columns(4).Width = 500
                .Columns(5).Width = 500
                .Columns(6).Visible = False
                .Columns(7).Visible = False
                .Columns(8).Visible = False
                .Columns(9).Visible = False


                .Columns(3).HeaderText = "Question"
                .Columns(4).HeaderText = "Answer"
                .Columns(5).HeaderText = "Question Type"

            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub frm_questions_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        popDgvquestions()
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        Dim row As DataGridViewRow = DataGridView1.CurrentRow
        txttid.Text = row.Cells(1).Value.ToString
        txtques_id.Text = row.Cells(2).Value.ToString
        txtquest.Text = row.Cells(3).Value.ToString
        txtquesanswer.Text = row.Cells(4).Value.ToString
        txtquestype.Text = row.Cells(5).Value.ToString
        txta.Text = row.Cells(6).Value.ToString
        txtb.Text = row.Cells(7).Value.ToString
        txtc.Text = row.Cells(8).Value.ToString
        txtd.Text = row.Cells(9).Value.ToString
    End Sub

    Private Sub txttid_TextChanged(sender As Object, e As EventArgs) Handles txttid.TextChanged
        If txttid.Text = "" Then
            Button1.Enabled = False
            Button2.Enabled = False
        Else
            Button1.Enabled = True
            Button2.Enabled = True
        End If
    End Sub

    Private Sub txtquestype_TextChanged(sender As Object, e As EventArgs) Handles txtquestype.TextChanged
        If txtquestype.Text = "torf" Then
            GroupBox1.Visible = False
        ElseIf txtquestype.Text = "multiple" Then
            GroupBox1.Visible = True
        Else
            GroupBox1.Visible = False
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        update123123()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        del()
    End Sub
End Class