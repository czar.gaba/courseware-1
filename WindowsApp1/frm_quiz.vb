﻿Imports System.Data.OleDb
Public Class frm_quiz
    Dim a As Integer = 0
    Dim r As Double = 0
    Dim wrong As Double = 0
    Dim total As Double
    Dim percentage As Double
    Dim remainder As Double


    Sub loaddata()


        Try


            da = New OleDbDataAdapter("SELECT * FROM tbl_questions Where t_id='" & lblquiz.Text & "' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_questions")

            total = ds.Tables("tbl_questions").Rows.Count




            If (ds.Tables("tbl_questions").Rows.Count <> 0) Then

                lblquesid.Text = ds.Tables("tbl_questions").Rows(a).Item("ques_id")
                lblanswer.Text = ds.Tables("tbl_questions").Rows(a).Item("ques_answer")
                lblquestype.Text = ds.Tables("tbl_questions").Rows(a).Item("ques_type")
                lblquestion.Text = ds.Tables("tbl_questions").Rows(a).Item("ques_content")

                If lblquestype.Text = "multiple" Then
                    groupmulti.Visible = True
                    groupfill.Visible = False
                    grouptof.Visible = False

                    rba.Text = ds.Tables("tbl_questions").Rows(a).Item("choice_A")
                    rbb.Text = ds.Tables("tbl_questions").Rows(a).Item("choice_B")
                    rbc.Text = ds.Tables("tbl_questions").Rows(a).Item("choice_C")
                    rbd.Text = ds.Tables("tbl_questions").Rows(a).Item("choice_D")
                ElseIf lblquestype.Text = "torf" Then
                    groupmulti.Visible = False
                    groupfill.Visible = False
                    grouptof.Visible = True
                Else
                    groupmulti.Visible = False
                    groupfill.Visible = True
                    grouptof.Visible = False
                    txtfill.Focus()

                End If

            End If



        Catch ex As Exception
            da = New OleDbDataAdapter("SELECT * FROM tbl_questions Where t_id='" & lblquiz.Text & "' ", module1.conn)
            ds = New DataSet
            da.Fill(ds, "tbl_questions")

            total = ds.Tables("tbl_questions").Rows.Count


            percentage = lblright.Text * 100
            percentage = percentage / total

            frm_displayscore.lblscore.Text = percentage & "%" 'note if the quiz is done button on dashboard student should be hidden
            frm_displayscore.ShowDialog() ' get it done when u copy this code to the dashboard student
            Button4.PerformClick()
            a = 0
            r = 0
            wrong = 0


        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If lblquestype.Text <> "questype" Then
            If rba.Checked = True Then
                If rba.Text = lblanswer.Text Then
                    r = r + 1
                End If

            ElseIf rbb.Checked = True Then
                If rbb.Text = lblanswer.Text Then
                    r = r + 1
                End If

            ElseIf rbc.Checked = True Then
                If rbc.Text = lblanswer.Text Then
                    r = r + 1
                End If
            ElseIf rbd.Checked = True Then
                If rbd.Text = lblanswer.Text Then
                    r = r + 1
                End If
            ElseIf rbtrue.Checked = True Then
                If rbtrue.Text = lblanswer.Text Then
                    r = r + 1
                End If
            ElseIf rbfalse.Checked = True Then
                If rbfalse.Text = lblanswer.Text Then
                    r = r + 1
                End If
            ElseIf txtfill.Text.ToUpper() = lblanswer.Text.ToUpper() Then
                r = r + 1
            Else
                wrong = wrong + 1
            End If


        End If

        a = a + 1
        lblright.Text = r
        lblwrong.Text = wrong
        rba.Checked = False
        rbb.Checked = False
        rbc.Checked = False
        rbd.Checked = False
        rbtrue.Checked = False
        rbfalse.Checked = False
        txtfill.Clear()
        loaddata()



    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.Close()

    End Sub

    Private Sub frm_quiz_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblright.Text = 0
        lblwrong.Text = 0
        loaddata()

    End Sub
End Class